#!/bin/sh
###
## This script requires the DIGITALOCEAN_ACCESS_TOKEN variable be present
## This script runs inside the aoepeople/digital-ocean-helper:1.18.0 container which has doctl and kubectl installed
##

# load the kubeconfig for the cluster using the doctl cli
doctl kubernetes cluster kubeconfig save test-cluster-3

# identify the pod based on the deployment name
export POD=$(kubectl get pods -o name | grep -m1 $CI_COMMIT_REF_SLUG-admin | cut -d'/' -f 2)

# verify that the container's commit sha matches the ci commit sha before running tests
export CONTAINER_COMMIT_SHA=$(kubectl exec $POD printenv COMMIT_HASH)
if [ "$CI_COMMIT_SHA" != "$CONTAINER_COMMIT_SHA" ]; then
	 echo "Container Commit SHA ($CONTAINER_COMMIT_SHA) does not equal CI Commit SHA ($CI_COMMIT_SHA)"
	 exit 1
fi


# Run tests using phpdbg and phpunit.phar, generate html code coverage to the volume shared with nginx, and output coverage-text to be picked up by the ci-runner
kubectl exec $POD -- sh -c "APP_MODE=api; /usr/bin/phpdbg7 -qrr /usr/local/bin/phpunit --coverage-html /srv/www/public/coverage --coverage-text --colors=never --configuration phpunit.xml --testsuite Feature,Unit"
