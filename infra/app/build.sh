#!/bin/sh

##
# exit if a command returns a non-zero exit code and print the commands
# and their args as they are executed
#
set -e -x

##
# Define build variables
#
export TAG_LATEST=${CONTAINER}
export TAG_BUILD=${CI_COMMIT_SHORT_SHA}-${CONTAINER}
export DOCKERFILE_LOCATION=${BUILD_PATH}/${DOCKERFILE}
export CI_REGISTRY_IMAGE_BRANCH=${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}

##
# Enable experimental for manifest inspect
#
mkdir -p ~/.docker
echo '{"experimental": "enabled"}' > ~/.docker/config.json

##
# Login to the image repository
#
docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}

##
# Exit if a build with the current tag already exists
#
docker  manifest inspect ${CI_REGISTRY_IMAGE_BRANCH}:${TAG_BUILD} > /dev/null && exit || true

##
# Pull latest image to use as a cache for our build
#
docker pull ${CI_REGISTRY_IMAGE_BRANCH}:${TAG_LATEST} || true

# Build and tag the images:
#
#   docker build
#     -f ./nuxt/Dockerfile
#     -t gitlabuser/kaboodle/my-branch:b4e33311-nuxt
#     -t gitlabuser/kaboodle/my-branch:nuxt
#     --cache-from gitlabuser/kaboodle/my-branch:nuxt
#     ./nuxt
#
docker build \
  -f ${DOCKERFILE_LOCATION} \
  -t ${CI_REGISTRY_IMAGE_BRANCH}:${TAG_BUILD} \
  -t ${CI_REGISTRY_IMAGE_BRANCH}:${TAG_LATEST} \
  --cache-from ${CI_REGISTRY_IMAGE_BRANCH}:${TAG_LATEST} \
  ${BUILD_PATH}

##
# Push the build specific tag and update the branch's latest tag
#
docker push ${CI_REGISTRY_IMAGE_BRANCH}:${TAG_BUILD}
docker push ${CI_REGISTRY_IMAGE_BRANCH}:${TAG_LATEST}
