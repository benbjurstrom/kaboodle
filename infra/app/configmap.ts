import * as k8s from "@pulumi/kubernetes";
import * as random from "@pulumi/random";
import {ProviderResource} from "@pulumi/pulumi";
import * as config from "./config";
import * as app from '../templates/index'

export interface ConfigMaps {
    server: k8s.core.v1.ConfigMap
    nuxt: k8s.core.v1.ConfigMap
}

export async function createConfigMaps(
    k8sProvider: ProviderResource
): Promise<ConfigMaps> {

    /**
     * Define values obtained from pulumi's config
     */
    const appEnv = 'container'
    const commitBranch = config.commitBranch
    const commitDateTime = config.commitDateTime
    const commitHash = config.commitHash
    const commitMessage = config.commitMessage

    const databaseHost = config.databaseHost
    const databasePort = config.databasePort
    const databaseUser = config.databaseUser
    const databasePass = config.databasePass

    const redisPass = config.redisPass
    const tld = config.clusterTld

    /**
     * Define values based on the value of config.commitBranch
     */
    const databaseName = getServiceName('kaboodle')
    const apiHostInternal = getServiceName('api')
    const apiHostExternal = apiHostInternal + '.' + tld
    const clientHost = getServiceName('www')
    const websocketHostInternal = getServiceName('websocket')
    const websocketHostExternal = websocketHostInternal + '.' + tld
    const redisQueue = getServiceName('default')

    const server = await new k8s.core.v1.ConfigMap(getServiceName('server'), {
            metadata: {
                name: getServiceName('server'),
                labels: {
                    name: getServiceName('server')
                },
                namespace: 'default'
            },
            data: {
                APP_NAME:'Kaboodle',
                APP_ENV: appEnv,
                APP_KEY: new random.RandomString('APP_KEY', { length: 32, special: false }).result,
                APP_DEBUG:'true',
                APP_URL:'https://' + apiHostExternal,

                BROADCAST_DRIVER:'pusher',

                CACHE_DRIVER:'redis',

                CLIENT_URL_INTERNAL:'http://' + clientHost,

                COMMIT_BRANCH: commitBranch,
                COMMIT_DATETIME: commitDateTime,
                COMMIT_HASH: commitHash,
                COMMIT_MESSAGE: commitMessage,

                DB_CONNECTION:'pgsql',
                DB_HOST: databaseHost,
                DB_PORT: databasePort,
                DB_DATABASE: databaseName,
                DB_USERNAME: databaseUser,
                DB_PASSWORD: databasePass,

                JWT_SECRET: new random.RandomString('JWT_SECRET', { length: 64, special: false }).result,

                LOG_CHANNEL:'stack',

                MAIL_DRIVER:'smtp',
                MAIL_HOST: 'mailhog',
                MAIL_PORT:'1025',
                MAIL_USERNAME:'null',
                MAIL_PASSWORD:'null',
                MAIL_ENCRYPTION:'null',

                QUEUE_CONNECTION:'redis',
                QUEUE_NAME: redisQueue,

                REDIRECT_HTTPS: 'true',

                REDIS_QUEUE: redisQueue,
                REDIS_HOST:'redis-master',
                REDIS_PASSWORD: redisPass,
                REDIS_PORT:'6379',

                SELENIUM_HOST:'selenium-hub',

                SESSION_DRIVER:'redis',
                SESSION_CONNECTION:'default',
                SESSION_LIFETIME:'120',

                TELESCOPE_ENABLED:'true',

                WEBSOCKET_ID: commitBranch,
                WEBSOCKET_KEY: commitBranch,
                WEBSOCKET_SECRET: new random.RandomString('WEBSOCKET_SECRET', { length: 64, special: false }).result,
                WEBSOCKET_ENCRYPTED: 'true',

                /**
                 * Laravel interacts with the websocket's service from inside the cluster. The config/broadcast.php
                 * should be configured to target the hostname, port, and scheme defined on the websocket's service.
                 */
                WEBSOCKET_HOST_INTERNAL: websocketHostInternal,
                WEBSOCKET_PORT_INTERNAL:'80',

                /**
                 * The Debug Dashboard of the laravel-websockets package interacts with the websocket server from
                 * outside the cluster. The config/websockets.php should be configured to target the hostname and port
                 * defined on the websocket's ingress.
                 */
                WEBSOCKET_HOST_EXTERNAL: websocketHostExternal,
                WEBSOCKET_PORT_EXTERNAL: '443',
            }
        },
        {
            provider: k8sProvider
        });

    const nuxt = await new k8s.core.v1.ConfigMap(getServiceName('nuxt'), {
            metadata: {
                name: getServiceName('nuxt'),
                labels: {
                    name: getServiceName('nuxt')
                },
                namespace: 'default'
            },
            data: {
                /**
                 * Nuxt uses a proxy to interact with the api from inside the cluster. Nuxt's proxy settings
                 * in the nuxt.config.js file should be configured to target the hostname, port, and scheme defined in
                 * the api's service.
                 */
                API_HOST_INTERNAL: apiHostInternal,

                APP_ENV: appEnv,

                COMMIT_BRANCH: commitBranch,
                COMMIT_DATETIME: commitDateTime,
                COMMIT_HASH: commitHash,
                COMMIT_MESSAGE: commitMessage,

                /**
                 * Nuxt uses the web browser to interact with the websocket server from outside the cluster.
                 * Nuxt's  instantiation of laravel-echo should be configured to target the hostname and
                 * port defined on the websocket's ingress.
                 */
                WEBSOCKET_HOST_EXTERNAL: websocketHostExternal,
                WEBSOCKET_PORT_EXTERNAL: '443',
                WEBSOCKET_KEY: commitBranch
            }
        },
        {
            provider: k8sProvider,
            dependsOn: server
        });

    return {
        server: server,
        nuxt: nuxt
    }
}

function getServiceName(service: string): string {
    return config.commitBranch + '-' + service
}
