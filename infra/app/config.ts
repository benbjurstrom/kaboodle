import {Config} from "@pulumi/pulumi";

// Enviornment Specific Config
export const commitBranch = new Config('commit').require('branch');
export const commitDateTime = new Config('commit').require('datetime');
export const commitHash = new Config('commit').require('hash');
export const commitHashShort = new Config('commit').require('hashShort');
export const commitMessage = new Config('commit').require('message');
export const pulumiStack = new Config('pulumi').require('stack');
export const pulumiEnv = new Config('pulumi').require('env');
export const pulumiDown = new Config('pulumi').require('down');

// Cloudflare
export const cloudflareEmail = new Config('cloudflare').require('email');
export const cloudflareToken = new Config('cloudflare').require('token');

// Cluster Config
export const clusterName = new Config('cluster').require('name');
export const ipv4Address = new Config('cluster').require('ipv4Address');
export const clusterTld = new Config('cluster').require('tld');
export const whitelist = new Config('cluster').require('whitelist');

// Database Config
export const databaseName = new Config('database').require('name');
export const databaseHost = new Config('database').require('host');
export const databasePort = new Config('database').require('port');
export const databaseUser = new Config('database').require('username');
export const databasePass = new Config('database').require('password');

// Laravel App
export const laravelAppKey = new Config('laravel').require('appKey');

// Lets encrypt
export const letsencryptEmail = new Config('letsencrypt').require('email');
export const letsencryptServer = new Config('letsencrypt').require('server');
export const letsencryptName = new Config('letsencrypt').require('name');

// Registry config
export const registryHostname = new Config('registry').require('hostname');
export const registryImage = new Config('registry').require('image');
export const registryPassword = new Config('registry').require('password');
export const registryProjectPath = new Config('registry').require('projectPath');

// Redis
export const redisPass = new Config('redis').require('password');
