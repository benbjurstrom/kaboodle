#!/bin/bash
# This script depends the following environmental variables
# - PULUMI_ORGANIZATION
# - PULUMI_PROJECT
# - PULUMI_CONFIG_PASSPHRASE

# exit if a command returns a non-zero exit code and also print the commands and their args as they are executed
set -e -x

# Add the pulumi CLI to the PATH
export PATH=$PATH:$HOME/.pulumi/bin

# install packages
yarn --cwd ../ install --check-files

pulumi plugin install
pulumi plugin ls

PULUMI_STACK="${PULUMI_ORGANIZATION}/${CI_COMMIT_REF_SLUG}"

{ # try
    # Init a new stack called $PULUMI_STACK using the passphrase set in PULUMI_CONFIG_PASSPHRASE
    pulumi stack init $PULUMI_STACK --secrets-provider passphrase
} || { # catch
    # If $PULUMI_STACK already exists select it instead
    pulumi stack select $PULUMI_STACK
}

# Set environment specific variables
pulumi config set commit:branch $CI_COMMIT_REF_SLUG --config-file Pulumi.app.yaml
pulumi config set commit:hash $CI_COMMIT_SHA --config-file Pulumi.app.yaml
pulumi config set commit:hashShort $CI_COMMIT_SHORT_SHA --config-file Pulumi.app.yaml
pulumi config set commit:message "'$CI_COMMIT_TITLE'" --config-file Pulumi.app.yaml
pulumi config set commit:datetime "$(date +%FT%T%z)" --config-file Pulumi.app.yaml
pulumi config set pulumi:stack $PULUMI_STACK --config-file Pulumi.app.yaml
pulumi config set pulumi:env ci --config-file Pulumi.app.yaml
pulumi config set registry:image ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG} --config-file Pulumi.app.yaml
pulumi config set registry:projectPath ${CI_PROJECT_PATH_SLUG} --config-file Pulumi.app.yaml

# Run pulumi

##
# If down is passed into the script then delete the gitlab registry
# repository for this branch, drop the database, remove the deployment,
# and delete the now unused pulumi stack.
if [ "$1" == "down" ]; then
    # Deploys a job that will run artisan gitlab:delete-registry followed by
    # artisan db:drop
    pulumi config set pulumi:down yes --config-file Pulumi.app.yaml
    pulumi up --config-file Pulumi.app.yaml
    sleep 10s

    # Then run pulumi destroy and remove the now unused stack
    pulumi destroy --config-file Pulumi.app.yaml
    pulumi stack rm --non-interactive --yes
else
    pulumi up --config-file Pulumi.app.yaml
fi
