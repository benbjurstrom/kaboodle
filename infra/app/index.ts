import * as config from './config';
import * as k8s from '@pulumi/kubernetes';
import * as digitalocean from '@pulumi/digitalocean';
import * as app from '../templates/index'
import * as cm from './configmap'
import { ProviderResource } from '@pulumi/pulumi';
import * as container from "../templates/container";
import {getPodSpec} from "../templates/deployment";
import * as d from "../templates/deployment";
import {Env} from "../templates/container";

async function main() {

    // Use the cluster name to obtain the k8sProvider
    const cluster = await digitalocean.getKubernetesCluster({ name: config.clusterName });
    const k8sProvider = new k8s.Provider( config.clusterName, {
        kubeconfig: cluster.kubeConfigs[0].rawConfig
    });

    const configMaps = await cm.createConfigMaps(k8sProvider)

    if(config.pulumiDown === 'yes'){
        deployTeardownJob(k8sProvider, configMaps)
    }else{
        deployMigrationJob(k8sProvider, configMaps)
    }

    deployJobsHeartbeat(k8sProvider, configMaps)
    deployAdmin(k8sProvider, configMaps)
    deployApi(k8sProvider, configMaps)
    deployHorizon(k8sProvider, configMaps)
    deployQueueWorker(k8sProvider, configMaps)
    deployWebsocket(k8sProvider, configMaps)
    deployClient(k8sProvider, configMaps)

    return {
        clusterName: config.clusterName
    };
}

/**
 * Deploys a laravel container as a k8s job which creates the database, runs the migrations, and seeds the database.
 * @k8sProvider
 * @configMaps
 */
function deployMigrationJob(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps){
    deployJob(
        k8sProvider,
        configMaps,
        ['composer install && php artisan db:create && php artisan migrate:fresh --seed'],
        [{ name: 'APP_MODE', value: 'admin' }]
    )
}

/**
 * Deploys a laravel container as a k8s job which deletes the registry containers for the current branch, drops the
 * database, removes the deployment, and deletes the now unused pulumi stack.
 * @k8sProvider
 * @configMaps
 */
function deployTeardownJob(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps){
    deployJob(
        k8sProvider,
        configMaps,
        ['php artisan gitlab:delete-registry && php artisan db:drop'],
        [
            { name: 'APP_MODE', value: 'admin' },
            { name: 'REGISTRY_PROJECT_PATH', value: config.registryProjectPath },
            { name: 'REGISTRY_PASSWORD', value: config.registryPassword },
        ]
    )
}

/**
 * Deploys a nuxt container to act as as the client server.
 * @k8sProvider
 * @configMaps
 */
function deployClient(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps){
    const name = getAppName('www')
    const tag  = getBuildTagName('nuxt')

    const options: app.Options = {
        ingress: true,
        ingressWhitelist: false,
        deploymentReplicas: 1,
        serviceTargetPort: 3000
    }

    let container: container.Container = {
        name: name,
        image: `${config.registryImage}:${tag}`,
        envFrom:  [ { configMapRef: { name: configMaps.nuxt.metadata.apply(m => m.name) }}],
    }
    app.createApp(k8sProvider, name, container, options, config)
}

/**
 * Deploys a laravel container to act as as the api server.
 * @k8sProvider
 * @configMaps
 */
function deployApi(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps){
    const name = getAppName('api')
    const tag  = getBuildTagName('laravel')

    const options: app.Options = {
        ingress: true,
        ingressWhitelist: false,
        deploymentReplicas: 1,
        deploymentNginxSidecar: true,
        serviceTargetPort: 80
    }

    let container: container.Container = {
        name: name,
        image: `${config.registryImage}:${tag}`,
        env:  [{ name: 'APP_MODE', value: 'api' }],
        envFrom:  [ { configMapRef: { name: configMaps.server.metadata.apply(m => m.name) }}]
    }
    app.createApp(k8sProvider, name, container, options, config)
}

/**
 * Deploys a laravel container as a k8s cronjob which runs the artisan schedule:run command every five minutes.
 * @k8sProvider
 * @configMaps
 */
function deployJobsHeartbeat(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps){
    const name = getAppName('heartbeat')
    const tag  = getBuildTagName('laravel')

    let container: container.Container = {
        name: name,
        image: `${config.registryImage}:${tag}`,
        env:  [{ name: 'APP_MODE', value: 'admin' }],
        envFrom:  [ { configMapRef: { name: configMaps.server.metadata.apply(m => m.name) }}],
        args: ['php artisan schedule:run'],
        command: ['/bin/sh','-c'],
        workingDir: '/var/www'
    }

    let podSpec = getPodSpec(name, container)
    podSpec.restartPolicy = 'OnFailure'


    new k8s.batch.v1beta1.CronJob(name, {
            metadata: {
                name: name,
            },
            spec: {
                schedule: "*/5 * * * *",
                jobTemplate: {
                    spec: {
                        template: {
                            spec: podSpec
                        }
                    }
                }
            }
        },
        {
            provider: k8sProvider,
            deleteBeforeReplace: true
        }
    );
}

/**
 * Deploys a laravel container to act as as the admin server.
 * @k8sProvider
 * @configMaps
 */
function deployAdmin(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps){
    const name = getAppName('admin')
    const tag  = getBuildTagName('laravel')

    const options: app.Options = {
        ingress: true,
        ingressWhitelist: true,
        deploymentReplicas: 1,
        deploymentNginxSidecar: true,
        serviceTargetPort: 80
    }

    let container: container.Container = {
        name: name,
        image: `${config.registryImage}:${tag}`,
        env:  [{ name: 'APP_MODE', value: 'admin' }],
        envFrom:  [ { configMapRef: { name: configMaps.server.metadata.apply(m => m.name) }}]
    }
    app.createApp(k8sProvider, name, container, options, config)
}

/**
 * Deploys a laravel container to act as a queue worker.
 * @k8sProvider
 * @configMaps
 */
function deployQueueWorker(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps){
    const name = getAppName('queue')
    const tag  = getBuildTagName('laravel')

    const options: app.Options = {
        ingress: false,
        deploymentReplicas: 1,
        serviceTargetPort: 80
    }

    let container: container.Container = {
        name: name,
        image: `${config.registryImage}:${tag}`,
        env:  [{ name: 'APP_MODE', value: 'queue' }],
        envFrom:  [ { configMapRef: { name: configMaps.server.metadata.apply(m => m.name) }}]
    }
    app.createApp(k8sProvider, name, container, options, config)
}

/**
 * Deploys a laravel container to act as a horizon worker.
 * @k8sProvider
 * @configMaps
 */
function deployHorizon(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps){
    const name = getAppName('horizon')
    const tag  = getBuildTagName('laravel')

    const options: app.Options = {
        ingress: false,
        deploymentReplicas: 1,
        serviceTargetPort: 80
    }

    let container: container.Container = {
        name: name,
        image: `${config.registryImage}:${tag}`,
        env:  [{ name: 'APP_MODE', value: 'horizon' }],
        envFrom:  [ { configMapRef: { name: configMaps.server.metadata.apply(m => m.name) }}]
    }
    app.createApp(k8sProvider, name, container, options, config)
}

/**
 * Deploys a laravel container to act as a a websocket server.
 * @k8sProvider
 * @configMaps
 */
function deployWebsocket(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps){
    const name = getAppName('websocket')
    const tag  = getBuildTagName('laravel')

    const options: app.Options = {
        ingress: true,
        deploymentReplicas: 1,
        deploymentNginxSidecar: true,
        serviceTargetPort: 6001
    }

    let container: container.Container = {
        name: name,
        image: `${config.registryImage}:${tag}`,
        env:  [{ name: 'APP_MODE', value: 'websocket' }],
        envFrom:  [ { configMapRef: { name: configMaps.server.metadata.apply(m => m.name) }}]
    }
    app.createApp(k8sProvider, name, container, options, config)
}

/**
 * Deploys a laravel container as a k8s job and runs the given args. Used for running migrations,
 * creating and dropping databases, deleting old docker registries, etc.
 * @k8sProvider
 * @configMaps
 * @args
 */
function deployJob(k8sProvider: ProviderResource, configMaps: cm.ConfigMaps, args: string[], env: Env[]){
    const name = getAppName('migration')
    const tag  = getBuildTagName('laravel')

    let container: container.Container = {
        name: name,
        image: `${config.registryImage}:${tag}`,
        env:  env,
        envFrom:  [ { configMapRef: { name: configMaps.server.metadata.apply(m => m.name) }}],
        args: args,
        command: ['/bin/sh','-c'],
        workingDir: '/var/www'
    }

    let podSpec = d.getPodSpec(name, container)
    podSpec.restartPolicy = 'Never'

    return new k8s.batch.v1.Job(name, {
            metadata: {
                name: name,
            },
            spec: {
                parallelism: 1,
                completions: 1,
                backoffLimit: 4,
                ttlSecondsAfterFinished: 100,
                template: {
                    spec: podSpec
                },
            }
        },
        {
            provider: k8sProvider,
            deleteBeforeReplace: true
        }
    );
}

/**
 * Returns the given app name pre-pended with the commit branch
 * @appName
 * @returns
 */
function getAppName(appName: string): string {
    // if config.tagged
    // if(config.commitBranch === 'master'){
        // return appName
    // }

    return config.commitBranch + '-' + appName
}

/**
 * Returns the given tag base name prepended with the commit hash. This is
 * used to identify the most recent build.
 *
 * @appName
 */
function getBuildTagName(tag: string){
   // Return simple tag if running outside of the ci pipeline
    if(config.pulumiEnv === 'local'){
       return tag
   }

    return config.commitHashShort + '-' + tag
}

module.exports = main();
