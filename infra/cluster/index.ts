import * as k8s from "@pulumi/kubernetes";

import * as c from "./cluster";
import {Config} from "@pulumi/pulumi";
import * as ic from "./ingress-controller";

async function main() {

    const cluster        = await c.createCluster();
    const kubeconfig     = cluster.kubeConfigs[0].rawConfig;
    const k8sProvider    = new k8s.Provider('cluster-provider', {kubeconfig});
    const ingressService = await ic.createIngressController(k8sProvider);

    return {
        ipv4address: ingressService.status.apply(status => status.loadBalancer.ingress[0].ip),
        clusterName: new Config('cluster').require('name'),
        clusterTld: new Config('cluster').require('tld')
    };

}
module.exports = main();
