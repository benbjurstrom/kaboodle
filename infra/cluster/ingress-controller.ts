import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export async function createIngressController(k8sProvider: pulumi.ProviderResource): Promise<k8s.core.v1.Service> {
    //https://pulumi.io/quickstart/aws/tutorial-eks/
    const ingressConfig = new k8s.yaml.ConfigFile('ingress-nginx-mandatory',
        {
            file: 'https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.24.1/deploy/mandatory.yaml',
            transformations: [
                (obj: any) => {
                    // Add proxy protocol to the nginx-configuration ConfigMap
                    // https://github.com/kubernetes/ingress-nginx/issues/3996#issuecomment-495451028
                    if (obj.kind == 'ConfigMap' && obj.metadata.name == 'nginx-configuration') {
                        obj.data = {}
                        obj.data['compute-full-forwarded-for'] = 'true'
                        obj.data['use-forwarded-headers'] = 'true'
                        obj.data['use-proxy-protocol'] = 'true'
                    }
                }
            ],
        },
        {
            providers: {
                kubernetes: k8sProvider
            },
        },
    );

    return await new k8s.core.v1.Service('ingress-nginx',
        {
            metadata: {
                name: 'ingress-nginx',
                namespace: 'ingress-nginx',
                labels: {
                    'app.kubernetes.io/name': 'ingress-nginx',
                    'app.kubernetes.io/part-of': 'ingress-nginx'
                },
                annotations: {
                    'nginx.ingress.kubernetes.io/from-to-www-redirect': 'true',
                    'service.beta.kubernetes.io/do-loadbalancer-enable-proxy-protocol': 'true'
                    // https://www.digitalocean.com/docs/kubernetes/how-to/configure-load-balancers/#proxy-protocol
                    // https://github.com/kubernetes/ingress-nginx/issues/3996
                },
            },
            spec: {
                externalTrafficPolicy: 'Local',
                type: 'LoadBalancer',
                selector: {
                    'app.kubernetes.io/name': 'ingress-nginx',
                    'app.kubernetes.io/part-of': 'ingress-nginx'
                },
                ports: [
                    {
                        name: 'http',
                        nodePort: 31269,
                        port: 80,
                        protocol: 'TCP',
                        targetPort: 'http',
                    },
                    {
                        name: 'https',
                        nodePort: 31299,
                        port: 443,
                        protocol: 'TCP',
                        targetPort: 'https',
                    },
                ]
            },
        },
        {
            provider: k8sProvider,
            dependsOn: ingressConfig
        }
    );
}
