import { Config } from "@pulumi/pulumi";
import * as digitalocean from "@pulumi/digitalocean";

export async function createCluster(){
    const name = new Config('cluster').require('name');
    const tag = new digitalocean.Tag(name, {});
    return new digitalocean.KubernetesCluster(name, {
        name: name,
        region: 'sfo2',
        version: '1.14.1-do.3',
        tags: [tag.id],
        nodePool: {
            name: 'worker-pool',
            size: 's-1vcpu-2gb',
            nodeCount: 3
        }
    });
}
