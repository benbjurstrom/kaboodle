import * as cloudflare from "@pulumi/cloudflare";
import * as config from "./config";

export async function createDns(
    aRecord: string
) {
    await new cloudflare.Record(config.clusterTld, {
        domain: config.clusterTld,
        name: aRecord + '.' + config.clusterTld,
        ttl: 3600,
        type: "A",
        value: config.clusterIpv4Address,
    })
}
