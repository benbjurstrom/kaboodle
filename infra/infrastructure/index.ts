import * as k8s from '@pulumi/kubernetes'
import * as digitalocean from '@pulumi/digitalocean'

import * as config from './config'
import * as app from '../templates'
import * as container from '../templates/container'
import * as r from './services/redis'
import * as s from './services/selenium'
import * as d from './services/dashboard'
import * as dns from './dns'
import * as l from './services/logging'
import * as m from './services/mailhog'
import * as cm from './certificates/certmanager'
import * as dcj from './secrets/dockerconfigjson'
import * as cak from './secrets/cloudflareapikey'
import * as ci from './certificates/clusterissuer'
import * as cert from './certificates/certificate'
import * as ncm from './configs/nginx-configmap'

// pulumi stack init app --secrets-provider passphrase
// $PULUMI_CONFIG_PASSPHRASE

async function main() {
    /**
     * Create wildcard dns entry
     */
    await dns.createDns( '*')


    /**
     * Use the cluster name to obtain the k8sProvider
     */
    const cluster = await digitalocean.getKubernetesCluster({ name: config.clusterName })
    const k8sProvider = new k8s.Provider( config.clusterName, {
        kubeconfig: cluster.kubeConfigs[0].rawConfig
    })

    /**
     * Deploy cluster secrets
     */
    await cak.createCloudflareApiKey(k8sProvider)
    await dcj.createDockerconfigjson(k8sProvider, config.registryHostname)

    /**
     * Deploy cluster configs
     */
    await ncm.createNginxConfigMap(k8sProvider)


    /**
     * Deploy cert-manager, clusterissuer, and our wildcard certificate
     */
    await cm.createCertManager(k8sProvider)
    const clusterIssuer = await ci.createIssuer(k8sProvider)
    await cert.createCertificate(k8sProvider)

    /**
     * Deploy general cluster services
     */
    // await l.deployEfk(k8sProvider)
    // d.createDashboard(k8sProvider)
    r.deployRedis(k8sProvider, 'redis')
    s.deploySelenium(k8sProvider)
    m.deployMailhog(k8sProvider)

    /**
     * Deploy echo1 and echo2 to test that everything is working
     */

    const options: app.Options = {
        ingress: true,
        ingressWhitelist: false,
        deploymentReplicas: 1,
        serviceTargetPort: 5678
    }

    let container: container.Container = {
        name: 'echo1',
        image: 'registry.hub.docker.com/hashicorp/http-echo:latest',
        args:  [ '-text=echo1' ]
    }
    await app.createApp(k8sProvider, 'echo1', container, options, config)

    container.name = 'echo2'
    container.args = ['-text=echo2']
    await app.createApp(k8sProvider, 'echo2', container, options, config)

    return {
        //
    }

}
module.exports = main()
