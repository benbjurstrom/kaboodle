import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as config from "../config";

/**
 * Create a dockerconfigjson secret to authenticate with the container registry
 */
export async function createDockerconfigjson(k8sProvider: pulumi.ProviderResource, registryHostname: string){
    const dockerconfigjson = {
        auths:{
            registryHostname:
                {
                    username: config.registryUsername,
                    password: config.registryPassword
                }
        }
    }

    return await new k8s.core.v1.Secret(`${registryHostname}-secret`, {
            metadata: {
                name: registryHostname,
            },
            type: 'kubernetes.io/dockerconfigjson',
            data: {
                '.dockerconfigjson': Buffer.from(JSON.stringify(dockerconfigjson)).toString("base64"),
            },
        },
        {
            provider: k8sProvider
        }
    );
}
