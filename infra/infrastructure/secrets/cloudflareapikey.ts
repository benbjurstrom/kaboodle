import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as config from "../config";

/**
 * Create a cloudflare-api-key secret which the ClusterIssuer uses to perform dns validation
 */
export async function createCloudflareApiKey(k8sProvider: pulumi.ProviderResource){
    return await new k8s.core.v1.Secret('cloudflare-api-key', {
            metadata: {
                name: 'cloudflare-api-key',
                namespace: 'cert-manager',
            },
            type: 'Opaque',
            data: {
                'api-key.txt': Buffer.from(config.cloudflareToken).toString("base64"),
            },
        },
        {
            provider: k8sProvider
        }
    );
}
