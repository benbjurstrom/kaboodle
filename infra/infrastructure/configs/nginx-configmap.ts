import * as k8s from "@pulumi/kubernetes";
import {ProviderResource} from "@pulumi/pulumi";
import * as config from "../../app/config";
import * as fs from "fs";

export async function createNginxConfigMap(
    k8sProvider: ProviderResource
) {
    return await new k8s.core.v1.ConfigMap('nginx-config', {
            metadata: {
                name: 'nginx-config',
                labels: {
                    name: 'nginx-config'
                },
                namespace: 'default'
            },
            data: {
                // https://github.com/kieranajp/k8s-php-fpm/blob/master/kubernetes-config/nginx-configmap.yaml
                // https://matthewpalmer.net/kubernetes-app-developer/articles/php-fpm-nginx-kubernetes.html
                'nginx.conf': fs.readFileSync('configs/nginx.conf').toString() }
        },
        {
            provider: k8sProvider
        });
}
