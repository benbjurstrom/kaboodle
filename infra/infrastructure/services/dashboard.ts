import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as digitalocean from "@pulumi/digitalocean";

/**
 * Installs the k8s dashboard based on this guide:
 *  - https://alexanderzeitler.com/articles/enabling-the-kubernetes-dashboard-for-digitalocean-kubernetes/
 *
 *  To access the dashboard follow these steps:
 *  - kubectl proxy -p 8080
 *  - http://localhost:8080/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
 *  - kubectl get secret -n kube-system
 *  - kubectl describe secret administrator-token-token-xxxxx -n kube-system
 *
 */
export async function createDashboard(k8sProvider: pulumi.ProviderResource) {
    const serviceAccountName = 'administrator-token'
    const sa = await new k8s.core.v1.ServiceAccount(serviceAccountName,
        {
            metadata: {
                name: serviceAccountName,
                namespace: 'kube-system'
            }
        },
        {
            provider: k8sProvider
        }
    );

    const rbac = await new k8s.rbac.v1beta1.ClusterRoleBinding(serviceAccountName,
    {
            metadata: {
                name: serviceAccountName,
                namespace: 'kube-system'
            },
            roleRef: {
                apiGroup: 'rbac.authorization.k8s.io',
                kind: 'ClusterRole',
                name: 'cluster-admin',
            },
            subjects: [
                {
                    kind: 'ServiceAccount',
                    name: serviceAccountName,
                    namespace: 'kube-system'
                }
            ]
        },
        {
            provider: k8sProvider,
            dependsOn: sa
        }
    );

    await new k8s.yaml.ConfigFile(
        'https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml',
        {},
        {
            providers: {
                kubernetes: k8sProvider
            },
            dependsOn: rbac
        }
    );
}
