import * as k8s from '@pulumi/kubernetes';
import {ProviderResource} from '@pulumi/pulumi';

export async function deploySelenium(
    k8sProvider: ProviderResource,
) {

    let name = 'selenium-hub'
    const hubService = await new k8s.core.v1.Service(name,
        {
            metadata: {
                name: name,
                namespace: 'default'
            },
            spec: {
                type: 'NodePort',
                sessionAffinity: 'None',
                ports: [
                    {
                        port: 4444,
                        targetPort: 4444,
                        name: 'port0'
                    }
                ],
                selector: {
                    app: name
                }
            }
        },
        {
            provider: k8sProvider
        }
    )

    const hubDeployment = await new k8s.apps.v1.Deployment(name, {
            metadata: {
                name: name,
                labels: {
                    name: name
                },
                namespace: 'default'
            },
            spec: {
                selector: {
                    matchLabels: {
                        app: name,
                    }
                },
                replicas: 1,
                template: {
                    metadata: {
                        labels: {
                            app: name,
                        }
                    },
                    spec: {
                        containers: [
                            {
                                name: name,
                                image: 'registry.hub.docker.com/selenium/hub:3.12.0-americium',
                                imagePullPolicy: 'Always',
                                ports: [
                                    {
                                        containerPort: 4444,
                                    },
                                ],
                                livenessProbe: {
                                    httpGet: {
                                        path: '/wd/hub/status',
                                        port: 4444
                                    },
                                    initialDelaySeconds: 30,
                                    timeoutSeconds: 5
                                },
                                readinessProbe: {
                                    httpGet: {
                                        path: '/wd/hub/status',
                                        port: 4444
                                    },
                                    initialDelaySeconds: 30,
                                    timeoutSeconds: 5
                                }
                            }
                        ]
                    }
                }
            }
        },
        {
            provider: k8sProvider
        });

    name = 'selenium-node-chrome'
    const nodeDeployment = await new k8s.apps.v1.Deployment(name, {
            metadata: {
                name: name,
                labels: {
                    name: name
                },
                namespace: 'default'
            },
            spec: {
                selector: {
                    matchLabels: {
                        app: name,
                    }
                },
                replicas: 1,
                template: {
                    metadata: {
                        labels: {
                            app: name,
                        }
                    },
                    spec: {
                        volumes: [
                            {
                                name: 'dshm',
                                emptyDir: {
                                    medium: 'Memory'
                                }
                            }
                        ],
                        containers: [
                            {
                                name: name,
                                image: 'selenium/node-chrome-debug:3.141',
                                ports: [
                                    {
                                        'containerPort': 5900
                                    }
                                ],
                                volumeMounts: [
                                    {
                                        mountPath: '/dev/shm',
                                        name: 'dshm'
                                    }
                                ],
                                env: [
                                    {
                                        name: 'HUB_HOST',
                                        value: 'selenium-hub'
                                    },
                                    {
                                        name: 'HUB_PORT',
                                        value: '4444'
                                    }
                                ],
                            }
                        ]
                    }
                }
            }
        },
        {
            provider: k8sProvider
        });

    return nodeDeployment
}
