import * as k8s from '@pulumi/kubernetes';
import {ProviderResource, Output} from '@pulumi/pulumi';
import * as config from "../config";

export async function deployMailhog(
    k8sProvider: ProviderResource
) {

    // https://github.com/codecentric/helm-charts
    return new k8s.helm.v2.Chart(
        'mailhog',
        {
            chart: 'mailhog',
            fetchOpts: {
                repo: 'https://codecentric.github.io/helm-charts'
            },
            values: {
                service: {
                    type: 'NodePort'
                },
                ingress: {
                    enabled: true,
                    annotations: {
                        'kubernetes.io/ingress.class': 'nginx',
                        'certmanager.k8s.io/cluster-issuer': config.letsencryptName,
                        'nginx.ingress.kubernetes.io/whitelist-source-range': config.whitelist
                    },
                    hosts: [
                        {
                            host: 'mailhog.' + config.clusterTld
                        }
                    ],
                    tls: [
                        {
                            hosts:[
                                 'mailhog.' + config.clusterTld,
                            ],
                            secretName: 'letsencrypt-wildcard-tls'
                        }
                    ]
                },
            },
            transformations:[
                (obj: any) => {
                    // Set externalTrafficPolicy = 'Local' to forward the client ip for whitelisting
                    if (obj.kind == 'Service') {
                        obj.spec.externalTrafficPolicy = 'Local'
                    }

                    // Fix broken chart
                    if (obj.kind == 'Ingress') {
                        obj.spec.rules[0].http.paths = [
                            {
                                backend: {
                                    serviceName: 'mailhog',
                                    servicePort: 'http'
                                }
                            }
                        ]
                    }
                }
            ]
        },
        { providers: { kubernetes: k8sProvider }}
    );
}
