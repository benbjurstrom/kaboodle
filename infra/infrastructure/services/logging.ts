import * as k8s from '@pulumi/kubernetes';
import {ProviderResource} from '@pulumi/pulumi';

export async function deployEfk(
    k8sProvider: ProviderResource,
) {

    // https://www.digitalocean.com/community/tutorials/how-to-set-up-an-elasticsearch-fluentd-and-kibana-efk-logging-stack-on-kubernetes

    /*
    |--------------------------------------------------------------------------
    | Create Namespace
    |--------------------------------------------------------------------------
    |
    |
    */

    const namespace = 'kube-logging'
    const namespaceResource = await new k8s.core.v1.Namespace(namespace,
        {
            metadata: {
                name: namespace
            }
        })


    /*
    |--------------------------------------------------------------------------
    | Create Elasticsearch Service & StatefulSet
    |--------------------------------------------------------------------------
    |
    |
    */

    let name = 'es-cluster'
    const elasticsearchStatefulSet = await new k8s.apps.v1.StatefulSet(name,
        {
            metadata: {
                name,
                namespace,
            },
            spec: {
                serviceName: 'elasticsearch',
                replicas: 3,
                selector: {
                    matchLabels: {
                        app: 'elasticsearch'
                    }
                },
                template: {
                    metadata: {
                        labels: {
                            app: 'elasticsearch'
                        }
                    },
                    spec: {
                        containers: [
                            {
                                name: 'elasticsearch',
                                image: 'docker.elastic.co/elasticsearch/elasticsearch-oss:6.4.3',
                                resources: {
                                    limits: {
                                        cpu: '1000m'
                                    },
                                    requests: {
                                        cpu: '100m'
                                    }
                                },
                                ports: [
                                    {
                                        containerPort: 9200,
                                        name: 'rest',
                                        protocol: 'TCP'
                                    },
                                    {
                                        containerPort: 9300,
                                        name: 'inter-node',
                                        protocol: 'TCP'
                                    }
                                ],
                                volumeMounts: [
                                    {
                                        name: 'data',
                                        mountPath: '/usr/share/elasticsearch/data'
                                    }
                                ],
                                env: [
                                    {
                                        name: 'cluster.name',
                                        value: 'k8s-logs'
                                    },
                                    {
                                        name: 'node.name',
                                        valueFrom: {
                                            fieldRef: {
                                                fieldPath: 'metadata.name'
                                            }
                                        }
                                    },
                                    {
                                        name: 'discovery.zen.ping.unicast.hosts',
                                        value: 'es-cluster-0.elasticsearch,es-cluster-1.elasticsearch,es-cluster-2.elasticsearch'
                                    },
                                    {
                                        name: 'discovery.zen.minimum_master_nodes',
                                        value: '2'
                                    },
                                    {
                                        name: 'ES_JAVA_OPTS',
                                        value: '-Xms512m -Xmx512m'
                                    }
                                ]
                            }
                        ],
                        initContainers: [
                            {
                                name: 'fix-permissions',
                                image: 'busybox',
                                command: [
                                    'sh',
                                    '-c',
                                    'chown -R 1000:1000 /usr/share/elasticsearch/data'
                                ],
                                securityContext: {
                                    privileged: true
                                },
                                volumeMounts: [
                                    {
                                        name: 'data',
                                        mountPath: '/usr/share/elasticsearch/data'
                                    }
                                ]
                            },
                            {
                                name: 'increase-vm-max-map',
                                image: 'busybox',
                                command: [
                                    'sysctl',
                                    '-w',
                                    'vm.max_map_count=262144'
                                ],
                                securityContext: {
                                    privileged: true
                                }
                            },
                            {
                                name: 'increase-fd-ulimit',
                                image: 'busybox',
                                command: [
                                    'sh',
                                    '-c',
                                    'ulimit -n 65536'
                                ],
                                securityContext: {
                                    privileged: true
                                }
                            }
                        ]
                    }
                },
                volumeClaimTemplates: [
                    {
                        metadata: {
                            name: 'data',
                            labels: {
                                app: 'elasticsearch'
                            }
                        },
                        spec: {
                            accessModes: [
                                'ReadWriteOnce'
                            ],
                            storageClassName: 'do-block-storage',
                            resources: {
                                requests: {
                                    storage: '10Gi'
                                }
                            }
                        }
                    }
                ]
            }
        },
        {
            provider: k8sProvider,
            dependsOn: namespaceResource
        }
    )

    name = 'elasticsearch'
    const elasticsearchService = await new k8s.core.v1.Service(name,
        {
            metadata: {
                name,
                namespace,
                labels: {
                    app: 'elasticsearch'
                }
            },
            spec: {
                selector: {
                    app: 'elasticsearch'
                },
                clusterIP: 'None',
                ports: [
                    {
                        port: 9200,
                        name: 'rest'
                    },
                    {
                        port: 9300,
                        name: 'inter-node'
                    }
                ]
            }
        },
        {
            provider: k8sProvider,
            dependsOn: elasticsearchStatefulSet
        }
    )

    /*
    |--------------------------------------------------------------------------
    | Create Kibana Deployment and Service
    |--------------------------------------------------------------------------
    |
    |
    */

    name = 'kibana'
    const kibanaDeployment = new k8s.apps.v1.Deployment(name, {
            metadata: {
                name: name,
                labels: {
                    app: name
                },
                namespace
            },
            spec: {
                selector: {
                    matchLabels: {
                        app: name
                    }
                },
                replicas: 1,
                template: {
                    metadata: {
                        labels: {
                            app: name
                        }
                    },
                    spec: {
                        containers: [
                            {
                                name: 'kibana',
                                image: 'docker.elastic.co/kibana/kibana-oss:6.4.3',
                                resources: {
                                    limits: {
                                        cpu: '1000m'
                                    },
                                    requests: {
                                        cpu: '100m'
                                    }
                                },
                                env: [
                                    {
                                        name: 'ELASTICSEARCH_URL',
                                        value: 'http://elasticsearch:9200'
                                    }
                                ],
                                ports: [
                                    {
                                        containerPort: 5601
                                    }
                                ]
                            }
                        ]
                    }
                }
            }
        },
        {
            provider: k8sProvider,
            dependsOn: namespaceResource
        }
    )

    const kibanaService = await new k8s.core.v1.Service(name,
        {
            metadata: {
                name,
                namespace,
                labels: {
                    app: name
                }
            },
            spec: {
                selector: {
                    app: name
                },
                ports: [
                    {
                        port: 5601,
                    }
                ]
            }
        },
        {
            provider: k8sProvider,
            dependsOn: kibanaDeployment
        }
    )

    /*
    |--------------------------------------------------------------------------
    | Create Fluentd DaemonSet
    |--------------------------------------------------------------------------
    |
    |
    */


}
