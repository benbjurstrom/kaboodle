import * as k8s from "@pulumi/kubernetes";
import {ProviderResource} from "@pulumi/pulumi";
import * as config from "../config";

export async function deployRedis(
    k8sProvider: ProviderResource,
    name: string,
) {

    const service = await new k8s.core.v1.Service(name + '-master',
        {
            metadata: {
                name: name + '-master',
                labels: {
                    name: name,
                    role: 'master',
                    tier: 'backend',
                },
                namespace: 'default'
            },
            spec: {
                type: 'NodePort',
                externalTrafficPolicy: 'Local',
                ports: [
                    {
                        port: 6379,
                        targetPort: 6379
                    }
                ],
                selector: {
                    app: name,
                    role: 'master',
                    tier: 'backend'
                }
            }
        },
        {
            provider: k8sProvider
        }
    )

    const env = [
        { name: "REDIS_PASSWORD", value: config.redisPass }
    ]

    const deployment = await new k8s.apps.v1.Deployment(name + '-master', {
            metadata: {
                name: name + '-master',
                labels: {
                    name: name
                },
                namespace: 'default'
            },
            spec: {
                selector: {
                    matchLabels: {
                        app: name,
                        role: 'master',
                        tier: 'backend'
                    }
                },
                replicas: 1,
                template: {
                    metadata: {
                        labels: {
                            app: name,
                            role: 'master',
                            tier: 'backend'
                        }
                    },
                    spec: {
                        imagePullSecrets: [
                            {
                                name: 'gitlab'
                            }
                        ],
                        containers: [
                            {
                                name: name + '-master',
                                image: 'registry.hub.docker.com/bitnami/redis:latest',
                                imagePullPolicy: 'Always',
                                ports: [
                                    {
                                        containerPort: 6379,
                                    },
                                ],
                                env
                            }
                        ]
                    }
                }
            }
        },
        {
            provider: k8sProvider
        });

    return deployment
}
