import {Config} from "@pulumi/pulumi";

export const clusterName = new Config('cluster').require('name');
export const clusterIpv4Address = new Config('cluster').require('ipv4Address');
export const clusterTld = new Config('cluster').require('tld');
export const whitelist = new Config('cluster').require('whitelist');

export const letsencryptName = new Config('letsencrypt').require('name');
export const letsencryptEmail = new Config('letsencrypt').require('email');
export const letsencryptServer = new Config('letsencrypt').require('server');

export const cloudflareEmail = new Config('cloudflare').require('email');
export const cloudflareToken = new Config('cloudflare').require('token');

export const redisPass = new Config('redis').require('password');

export const registryHostname = new Config('registry').require('hostname');
export const registryUsername = new Config('registry').require('username');
export const registryPassword = new Config('registry').require('password');
