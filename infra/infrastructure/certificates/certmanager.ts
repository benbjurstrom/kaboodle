import * as pulumi from '@pulumi/pulumi';
import * as k8s from '@pulumi/kubernetes';

// https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes#step-4-%E2%80%94-installing-and-configuring-cert-manager

/**
 * Sets up a certificate manager and a lets encrypt issuer. Based loosely off this guide:
 * - https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes
 *
 * Debug with:
 *  - kubectl describe ingress
 *  - kubectl describe clusterissuer letsencrypt-prod
 *  - kubectl describe certificate
 *  - kubectl describe order
 *  - kubectl describe challenge
 */
export async function createCertManager(k8sProvider: pulumi.ProviderResource): Promise<void> {
    const certmanager = await new k8s.yaml.ConfigFile(
        'https://github.com/jetstack/cert-manager/releases/download/v0.8.0/cert-manager.yaml',
        {},
        {
            providers: {
                kubernetes: k8sProvider
            }
        }
    );

    return
}
