import * as k8s from "@pulumi/kubernetes";
import {ProviderResource} from "@pulumi/pulumi";
import * as config from "../config";

// https://rimusz.net/lets-encrypt-wildcard-certs-in-kubernetes
export async function createCertificate(
    k8sProvider: ProviderResource
): Promise<k8s.apiextensions.CustomResource> {
    const namespace = 'default';
    const name = config.letsencryptName

    return await new k8s.apiextensions.CustomResource(name, {
        name,
        namespace,
        apiVersion: 'certmanager.k8s.io/v1alpha1',
        kind: 'Certificate',
        metadata: {
            name,
            namespace
        },
        spec: {
            secretName: 'letsencrypt-wildcard-tls',
            issuerRef: {
                name,
                kind: 'ClusterIssuer'
            },
            commonName: '*.' + config.clusterTld,
            acme: {
                config: [
                    {
                        dns01: {
                            provider: "cf-dns"
                        },
                        domains: [
                            '*.' + config.clusterTld
                        ]
                    }
                ]
            }
        }
    },
    {
        provider:k8sProvider
    });
}
