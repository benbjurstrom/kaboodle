import * as k8s from "@pulumi/kubernetes";
import { ProviderResource } from "@pulumi/pulumi";
import * as config from "../config";

export async function createIssuer(
    k8sProvider: ProviderResource,
): Promise<k8s.apiextensions.CustomResource>
{
    const name = config.letsencryptName;

    return await new k8s.apiextensions.CustomResource(name, {
        name,
        apiVersion: 'certmanager.k8s.io/v1alpha1',
        kind: 'ClusterIssuer',
        metadata: {
            name
        },
        spec: {
            acme: {
                server: config.letsencryptServer,
                email: config.letsencryptEmail,
                privateKeySecretRef: {
                    name
                },
                dns01: {
                    providers: [
                        {
                            name: 'cf-dns',
                            cloudflare: {
                                email: config.cloudflareEmail,
                                apiKeySecretRef: {
                                    name: 'cloudflare-api-key',
                                    key: 'api-key.txt'
                                }
                            }
                        }
                    ]
                }
            }
        }
    },
    {
        provider:k8sProvider
    });
}
