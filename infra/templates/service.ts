import * as k8s from "@pulumi/kubernetes";
import {ProviderResource} from "@pulumi/pulumi";

export async function createService(
    k8sProvider: ProviderResource,
    name: string,
    targetPort: number,
    deployment: k8s.apps.v1.Deployment
) {
    return await new k8s.core.v1.Service(name,
        {
            metadata: {
                name: name,
                labels: {
                    name: name
                },
                namespace: 'default'
            },
            spec: {
                type: 'NodePort',
                externalTrafficPolicy: 'Local',
                ports: [
                    {
                        port: 80,
                        targetPort
                    }
                ],
                selector: {
                    app: name
                }
            }
        },
        {
            provider: k8sProvider,
            dependsOn: deployment
        }
    )
}
