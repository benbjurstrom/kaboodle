import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import {output} from "@pulumi/pulumi";
import * as types from "@pulumi/kubernetes/types/output"

export interface Env {
    name: string,
    value: any
}

export interface Container {
    name: string,
    image: string,
    ports?: Object[],
    env?: Env[],
    args?: Object[],
    volumeMounts?: Object[],
    envFrom?: Object[]
    command?: any,
    workingDir?: string
}
