import * as k8s from "@pulumi/kubernetes";
import { ProviderResource, Output } from "@pulumi/pulumi";

export interface IngressAnnotations {
    [key: string]: any
}

export async function createIngress(
    k8sProvider: ProviderResource,
    name: string,
    service: k8s.core.v1.Service,
    config: any,
    whitelist?: boolean
){

    let annotations: IngressAnnotations = {
        'kubernetes.io/ingress.class': 'nginx',
        'certmanager.k8s.io/cluster-issuer': config.letsencryptName,
    }

    if(whitelist){
        annotations['nginx.ingress.kubernetes.io/whitelist-source-range'] = config.whitelist
    }

    return await new k8s.extensions.v1beta1.Ingress(name + '-ingress', {
            metadata: {
                name: name + '-ingress',
                annotations
            },
            spec: {
                tls: [
                    {
                        hosts:[
                            name + '.' + config.clusterTld,
                        ],
                        secretName: 'letsencrypt-wildcard-tls'
                    }
                ],
                rules: [
                    {
                        host: name + '.' + config.clusterTld,
                        http: {
                            paths: [
                                {
                                    backend: {
                                        serviceName: name,
                                        servicePort: 80
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        },
        {
            provider: k8sProvider,
            dependsOn: service
        }
    );
}
