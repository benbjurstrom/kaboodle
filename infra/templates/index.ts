import * as ingress from "./ingress";
import * as service from "./service";
import * as container from "./container";
import * as deployment from "../templates/deployment";
import { ProviderResource } from "@pulumi/pulumi";

export interface Options {
    deploymentReplicas?: number,
    deploymentNginxSidecar?: boolean,
    ingress: boolean,
    ingressWhitelist?: boolean,
    serviceTargetPort: number,
}

// Create the, deployment, service, and ingress
export async function createApp(
    k8sProvider: ProviderResource,
    name: string,
    container: container.Container,
    options: Options,
    config: object
){
    const deploymentResource = await deployment.createDeployment(k8sProvider, name, container, options.deploymentReplicas, options.deploymentNginxSidecar)
    const serviceResource = await service.createService(k8sProvider, name, options.serviceTargetPort, deploymentResource)

    // return early is the app will not be exposed via an ingress
    if(!options.ingress){
        return
    }

    await ingress.createIngress(k8sProvider, name, serviceResource, config, options.ingressWhitelist)
}
