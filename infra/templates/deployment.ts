import * as k8s from '@pulumi/kubernetes';
import * as app from './index';
import * as container from './container';
import { ProviderResource } from '@pulumi/pulumi';

export interface DeploymentOptions {
    namespace: string,
    replicas?: number,
    nginxSidecar?: boolean,
    configMap?: k8s.core.v1.ConfigMap
}

export async function createDeployment(
    k8sProvider: ProviderResource,
    name: string,
    container: container.Container,
    replicas?: number,
    nginxSidecar?: boolean
) {

    replicas = replicas || 1
    let namespace = 'default'

    return await new k8s.apps.v1.Deployment(name,
        {
            metadata: {
                name: name,
                labels: {
                    name: name
                },
                namespace: namespace
            },
            spec: {
                selector: {
                    matchLabels: {
                        app: name
                    }
                },
                replicas: replicas,
                template: {
                    metadata: {
                        labels: {
                            app: name
                        }
                    },
                    spec: getPodSpec(name, container, nginxSidecar)
                }
            }
        },
        {
            provider: k8sProvider,
        }
    )
}

export function getPodSpec(
    name: string,
    container: container.Container,
    nginxSidecar?: boolean
){
    // begin with a default pod spec
    let podSpec = {
        imagePullSecrets: [
            {
                name: 'registry.gitlab.com'
            }
        ],
        volumes: Array(),
        initContainers: Array(),
        containers: Array(),
        restartPolicy: 'Always'
    }

    // define the pod's primary container
    podSpec.containers.push({
        name: name,
        image: container.image,
        imagePullPolicy: 'Always',
        ports: container.ports,
        args: container.args,
        volumeMounts: container.volumeMounts || Array(),
        env: container.env,
        envFrom: container.envFrom || Array(),
        command: container.command,
        workingDir: container.workingDir
    })

    if( nginxSidecar ) addNginxSidecar(name, podSpec, container.image)

    return podSpec
}

/**
 * This method adds an nginx sidecar definition to the pod's spec.
 * This is the recommended way to interface with php-fpm while maintaining a single
 * process per container.
 */
function addNginxSidecar(name: string, podSpec: any, image: string){
    // Define two deployment volumes.
    // One for the php files that will be shared between nginx and phpfpm
    podSpec.volumes.push({
        name: 'shared-files',
        emptyDir: {}
    })

    // and another to hold the nginx config.
    podSpec.volumes.push({
        name: 'nginx-config-volume',
        configMap: {
            name: 'nginx-config'
        }
    })

    // push the shared-files volume onto the primary container
    podSpec.containers[0].volumeMounts.push({
        name: 'shared-files',
        mountPath: '/srv'
    })

    // add an init container that copies the applications php files to the shared-files volume
    podSpec.initContainers.push({
        name: name + '-init',
        image: image,
        imagePullPolicy: 'Always',
        command: ['/bin/sh','-c'],
        args: ['cp -r /var/www /srv && sudo chmod -R 777 /srv'],
        volumeMounts: [
            {
                name: 'shared-files',
                mountPath: '/srv'
            }
        ]
    })

    // finally add the nginx sidecar container to the deployment
    podSpec.containers.push({
        image: 'nginx:1.15.3-alpine',
        name: 'nginx',
        volumeMounts: [
            {
                name: 'shared-files',
                mountPath: '/srv'
            },
            {
                name: 'nginx-config-volume',
                mountPath: '/etc/nginx/nginx.conf',
                subPath: 'nginx.conf'
            }
        ]
    })
}
