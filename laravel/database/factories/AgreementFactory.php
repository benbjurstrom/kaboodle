<?php

use Faker\Generator as Faker;
use App\Models\Agreement;

$factory->define(Agreement::class, function (Faker $faker) {
    return [
        //
    ];
});

$factory->state(Agreement::class, 'privacy', [
    'content' => '# Minimam vivere meorum

## Diramque est

Lorem markdownum, caeli nube, ibi exorata coniugis gladio! Tuo tantum spem.
Deflent adempto est carmine! Et ordine habebat verticis, et praesaga ignes
quaesita superi **fratrem limenque**, et, inferiora ense: vultus bis. Quoque
terrae magnumque lignum maxima, [dare](http://manumque.net/), ora inmotus est
mora thalamos?

Abstulit fecere horrendaque **meis**, Corythi stabat; est viam. Quotiens illo:
*Iuppiter ut* victor audaces canis famulosque fugio relicta. *Feci pennas*,
reiectura ante Herculeamque tenent tellus, flammas, et cauda. Est leve, nate
candidus natum, ab et mora illa sumptis periclo, *si admota* sacra? Pervenit qui
annos quemquam, meum quodque admovet latus.

## Dicam est utque nec ignes animus si

Rex vates caelesti excussam insilit oculos, pande violave dixit gesserat
rapitur, illi perdant, seu plures. Mutat tantus cum hirsuti utque pecudumque
timorem.

> Ad sit putes qua inarata; flendoque silet, robora sine: Threicius verum,
> nostro *iubet Britannos* serae, variasque. Ordine superi neptis in huc per
> ferrum tendere fallit raptam. Iram non certamine hominemque arcum certa:
> liceret proceres incepto: avis iam. Et est suas; crescendo coluit et Penthea
> summe mediis instantes undas, de videtur.',
    'type' => 'PRIVACY',
    'source' => 'https://api.github.com/repos/digitalocean/tos/contents/privacy-policy.md',
]);

$factory->state(Agreement::class, 'terms', [
    'content' => '# Videant furiis

## Peteret pede

Lorem markdownum et vidit usus quique, venit faciemque, anxia dedero **et unus
Ditis** demitteret. Subit vertitis ab lacrimis sumpsit ulmi caede inquam cantus
*est tinguatur* vivitur.

## Lepores totumque ducta

Tenentes dederat in rapi sed erat **dictis** conspecta virgo corpora liliaque.
Et movit amnes signa tempore recurvo: deique fidissima Ceres et nomine patris
tunc sidera.

## Nil loricaeque omne officio

Et [saepe est delapsa](http://meorumiunctum.net/) erat dubito dixit Elide
corpora pelagi maerent me. Ore sol exclamant quoque velit, abit maiora! Quae
vale Actaeo curvataque Priameia illi clamore domino est sed torsit lecto!

> **Poteris iudice ora** stagnum Argo hostia modo egressu, medio
> [rudibusque](http://www.estvirum.net/et.aspx), aliquid datur illa vocato
> *dotatissima amore*: sub! In premunt centum? Est mirum et unum, tua [mei esse
> equo](http://dixit.com/) Tartara.',
    'type' => 'TERMS',
    'source' => 'https://api.github.com/repos/digitalocean/tos/contents/terms-of-service.md',
]);