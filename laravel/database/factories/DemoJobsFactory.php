<?php

use Faker\Generator as Faker;
use App\Models\DemoJob;
use App\Models\DemoJobStatus;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(DemoJob::class, function (Faker $faker) {
    $djs = DemoJobStatus::where('code', 'PENDING')->firstOrfail();
    return [
        'status_id' => $djs->id
    ];
});