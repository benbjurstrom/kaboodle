<?php

use Faker\Generator as Faker;
use App\Models\Notification;

$factory->define(Notification::class, function (Faker $faker) {
    return [
        'data' => ['message' => $faker->realText(80)]
    ];
});
