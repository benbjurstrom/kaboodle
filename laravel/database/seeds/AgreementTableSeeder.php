<?php

use Illuminate\Database\Seeder;
use App\Models\Agreement;

class AgreementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $privacy = factory(Agreement::class)->state('privacy')->create();
        $terms = factory(Agreement::class)->state('terms')->create();
    }
}
