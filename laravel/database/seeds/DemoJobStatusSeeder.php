<?php

use Illuminate\Database\Seeder;
use App\Models\DemoJobStatus;

class DemoJobStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $djs = new DemoJobStatus;
        $djs->name = 'Pending';
        $djs->code = 'PENDING';
        $djs->save();

        $djs = new DemoJobStatus;
        $djs->name = 'In Progress';
        $djs->code = 'IN_PROGRESS';
        $djs->save();

        $djs = new DemoJobStatus;
        $djs->name = 'Complete';
        $djs->code = 'COMPLETE';
        $djs->save();
    }
}
