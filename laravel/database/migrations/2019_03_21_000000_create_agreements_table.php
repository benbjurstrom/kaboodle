<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('source');
            $table->enum('type', ['PRIVACY', 'TERMS']);
            $table->char('sha', 40);
            $table->text('content');
            $table->timestamps();

            $table->unique('type');
        });

        \DB::statement("
            CREATE OR REPLACE FUNCTION calculate_agreement_content_hash() RETURNS trigger AS $$
            BEGIN
                IF tg_op = 'INSERT' OR tg_op = 'UPDATE' THEN
                    NEW.sha = ENCODE(DIGEST(NEW.content, 'sha1'), 'hex');
                    RETURN NEW;
                END IF;
            END;
            $$ LANGUAGE plpgsql;
        ");

        \DB::statement('
            CREATE TRIGGER trigger_calculate_agreement_content_hash
            BEFORE INSERT OR UPDATE ON agreements
            FOR EACH ROW EXECUTE PROCEDURE calculate_agreement_content_hash();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
