<?php

namespace Tests\Browser\Pages;

use App\Models\User;
use Facebook\WebDriver\Exception\TimeOutException;
use Laravel\Dusk\Browser;

class Login extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/login';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     * @throws TimeOutException
     */
    public function assert(Browser $browser)
    {
        $browser->waitForText('Login', 5)
            ->waitFor('.input')
            ->assertSee('Login');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@email' => 'input[name=email]',
            '@password' => 'input[name=password]',
        ];
    }

    /**
     * Login the user.
     *
     * @param  \Laravel\Dusk\Browser  $browser
     * @param  string  $email
     * @param  string  $password
     * @return void
     * @throws TimeOutException
     */
    public function doLogin(Browser $browser, $email, $password)
    {
        $browser
            ->resize(1920, 1080)
            ->waitFor('.input')
            ->type('email', $email)
            ->type('password', $password)
            ->press('Login')
            ->waitForText($email, 10)
            ->screenshot('homepage');
    }
}
