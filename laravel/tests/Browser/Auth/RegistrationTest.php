<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegistrationTest extends DuskTestCase
{
    public function testRegistrationSuccess()
    {
        $this->browse(function (Browser $browser) {
            $email = str_random(10) . '@example.com';
            $password = '123456789';

            $browser->visit('/auth/register')
                ->waitForText('Register New Account', 20)
                ->type('name', 'Test Name')
                ->type('email', $email)
                ->type('password', $password)
                ->check('terms')
                ->press('Register')
                ->waitForText('Email Verification Required', 20)
                ->assertSee('Email Verification Required');

            $this->assertDatabaseHas('users', [
                'email' => $email
            ]);
        });
    }
}
