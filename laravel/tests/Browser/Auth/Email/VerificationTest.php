<?php

namespace Tests\Browser;

use Tests\Browser\Pages\Login;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Services\AuthService;
use App\Models\User;

class VerificationTest extends DuskTestCase
{
    public function testVerifyEmail()
    {
        $this->browse(function (Browser $browser) {

            $as = new AuthService();
            $user = factory(User::class)->create([
                'password' => bcrypt('123'),
                'email_verified_at' => null
            ]);
            $this->assertFalse($user->hasVerifiedEmail());
            $signature = $as->getEmailVerificationSignature($user);

            $browser
                ->visit(new Login())
                ->doLogin($user->email, '123')
                ->visit('/')
                ->assertPathIs('/auth/email/unverified')
                ->press('Resend Email')
                ->waitForText('Success! A new email has been sent.')
                ->visit('/auth/email/verify?signature=' . $signature)
                ->waitForText($user->email)
                ->waitForLocation('/')
                ->assertPathIs('/');
        });
    }
}
