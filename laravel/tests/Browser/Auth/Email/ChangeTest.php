<?php

namespace Tests\Browser;

use Tests\Browser\Pages\Login;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Services\AuthService;
use App\Models\User;
use URL;

class ChangeTest extends DuskTestCase
{
    public function testChangeEmail()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)
                ->states(['withAgreements'])
                ->create(['password' => bcrypt('123')]);
            $new_email = $this->faker->email;

            $browser
                ->visit(new Login())
                ->doLogin($user->email, '123')
                ->visit('/auth/settings')
                ->assertPathIs('/auth/settings')
                ->type('email', $new_email)
                ->type('password', 123)
                ->press('Change email address')
                ->waitForText('Email Change Pending');

            $this->assertDatabaseHas('users', [
                'email_pending' => $new_email
            ]);
        });
    }

    public function testVerifyChange()
    {
        $this->browse(function (Browser $browser) {

            $new_email = $this->faker->email;
            $user = factory(User::class)
                ->states(['withAgreements'])
                ->create([
                    'password' => bcrypt('123'),
                    'email_pending' => $new_email
                ]);

            $as = new AuthService();
            $route = URL::signedRoute('auth.email.change.update', [
                'id' => $user->id,
                'email_pending' => $new_email
            ]);

            $browser
                ->visit(new Login())
                ->doLogin($user->email, '123')
                ->visit('/auth/email/change?route=' . base64_encode($route))
                ->waitForText('Change email address');

            $this->assertDatabaseHas('users', [
                'email' => $new_email
            ]);
        });
    }
}
