<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use App\Models\User;
use Password;

class ResetTest extends DuskTestCase
{
    public function testClientCanRequestPasswordReset()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->visit('/auth/reset')
                ->waitForText('Forgot Password', 10)
                ->type('email', $user->email)
                ->press('Send Reset Link')
                ->waitForText('Success', 10);

            $this->assertDatabaseHas('password_resets', [
                'email' => $user->email
            ]);
        });
    }

    public function testClientResetPassword()
    {
        $this->browse(function (Browser $browser) {

            $user       = factory(User::class)->create();
            $token      = Password::createToken($user);
            $password   = str_random(12);

            $browser->visit('/auth/reset/' . $token . '?email=' . $user->email)
                ->waitForText('Change Password', 10)
                ->type('password', $password)
                ->press('Save Password')
                ->waitForLocation('/', 10)
                ->assertPathIs('/');

            $success = (bool) auth()->attempt([
                'email'     => $user->email,
                'password'  => $password,
            ]);

            $this->assertTrue($success);
        });
    }
}
