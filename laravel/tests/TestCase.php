<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Routing\Middleware\ThrottleRequests;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, WithFaker, DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware(
            ThrottleRequests::class
        );
    }


    /**
     * @param object $instance
     * @param string $method
     * @param array $arguments
     * @return object
     * @throws \ReflectionException
     */
    protected function invokePrivateMethod($instance, $method, array $arguments = [])
    {
        $reflection = new \ReflectionClass($instance);
        $method = $reflection->getMethod($method);
        $method->setAccessible(true);
        return $method->invokeArgs($instance, $arguments);
    }
}
