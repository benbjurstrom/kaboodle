<?php

namespace Tests\Feature\Controllers\Api\Auth;

use App\Http\Resources\CurrentUserResource;
use App\Models\DemoJob;
use App\Models\User;
use Tests\TestCase;

class JobsControllerTest extends TestCase
{
    /**
     * GET Collection
     */
    public function testIndex()
    {
        $user = factory(User::class)->states(['withAgreements'])->create();
        auth()->login($user);

        $dj = factory(DemoJob::class)->create([
            'user_id' => $user->id
        ]);

        $this->getJson(route('demo.jobs.index'))
            ->assertStatus(200)
            ->assertJsonFragment(['user_id' => $user->id])
            ->assertJsonFragment(['status_code' => 'PENDING']);
    }

    /**
     * POST
     */
    public function testStore()
    {
        $user = factory(User::class)->states(['withAgreements'])->create();
        auth()->login($user);

        $this->postJson(route('demo.jobs.store'), [])
            ->assertStatus(201);
    }
}
