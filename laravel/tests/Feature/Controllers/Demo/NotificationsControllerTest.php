<?php

namespace Tests\Feature\Controllers\Api\Auth;

use App\Http\Resources\CurrentUserResource;
use App\Models\Notification;
use App\Notifications\DemoNotification;
use App\Models\User;
use Tests\TestCase;

class NotificationsControllerTest extends TestCase
{
    /**
     * GET Collection
     */
    public function testIndex()
    {
        $user = factory(User::class)->states(['withAgreements'])->create();
        auth()->login($user);

        $n = factory(Notification::class)->create([
            'notifiable_type' => User::class,
            'notifiable_id' => $user->id,
            'type' => DemoNotification::class
        ]);

        $this->getJson(route('demo.notifications.index'))
            ->assertStatus(200)
            ->assertJsonFragment(['notifiable_type' => User::class,])
            ->assertJsonFragment(['notifiable_id' => $user->id]);
    }

    /**
     * POST
     */
    public function testStore()
    {
        $user = factory(User::class)->states(['withAgreements'])->create();
        auth()->login($user);

        $this->postJson(route('demo.notifications.store'), [
            'message' => $this->faker->jobTitle
        ])
            ->assertStatus(202);
    }
}
