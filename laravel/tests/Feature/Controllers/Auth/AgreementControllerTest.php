<?php

namespace Tests\Feature\Controllers\Auth;

use App\Services\AgreementService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AgreementControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     * @throws
     */
    public function testExample()
    {
        $as = app()->make(AgreementService::class);
        $privacy = $as->getPrivacyAgreement();
        $terms = $as->getTermsAgreement();

        $this->getJson(route('auth.agreements.index', ['include_content' => true]))
            ->assertStatus(200)
            ->assertJsonFragment([
                'id'        => $privacy->id,
                'type'      => 'PRIVACY',
                'sha'       => $privacy->sha,
                'content'   => $privacy->content,
            ])
            ->assertJsonFragment([
                'id'        => $terms->id,
                'type'      => 'TERMS',
                'sha'       => $terms->sha,
                'content'   => $terms->content,
            ]);
    }
}
