<?php

namespace Tests\Feature\Controllers\Auth;

use App\Mail\EmailVerification;
use App\Models\Agreement;
use App\Models\User;
use App\Services\AgreementService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mail;

class RegistrationControllerTest extends TestCase
{
    /**
     * POST
     */
    public function testStore()
    {
        Mail::fake();

        $password = str_random(12);
        $user = factory(User::class)->make([
            'password' => bcrypt($password)
        ]);

        $as = app()->make(AgreementService::class);
        $privacy = $as->getPrivacyAgreement();
        $terms = $as->getTermsAgreement();

        $this->postJson(route('auth.register'), [
            'name'          => $user->name,
            'email'         => $user->email,
            'password'      => $password,
            'terms'         => true,
            'agreements'    => [
                'privacy'   => $privacy->sha,
                'terms'     => $terms->sha
            ],
        ])->assertStatus(201);

        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email,
        ]);

        Mail::assertQueued(EmailVerification::class);
    }
}
