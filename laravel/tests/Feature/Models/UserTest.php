<?php

namespace Tests\Feature\Models;

use App\Models\User;
use App\Models\DemoJobStatus;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{

    /**
     * Test the $user->demoJobs relationship
     *
     * @return void
     */
    public function testDemoJobsRelationship()
    {
        $user = factory(User::class)->create();
        $djs = DemoJobStatus::where('code', 'PENDING')->firstOrfail();

        $this->assertCount(0, $user->demoJobs);

        $user->demoJobs()->create(['status_id' => $djs->id]);
        $user->demoJobs()->create(['status_id' => $djs->id]);

        $this->assertCount(2, $user->fresh()->demoJobs);
    }

    /**
     * Test the $user->agreements relationship
     *
     * @return void
     */
    public function testAgreementRelationship()
    {
        $user = factory(User::class)
            ->states(['withAgreements'])
            ->create();

        $results = $user->agreements;
        $this->assertCount(2, $results);
    }
}
