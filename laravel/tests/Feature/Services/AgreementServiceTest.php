<?php

namespace Tests\Feature\Services;

use App\Models\User;
use App\Services\AuthService;
use App\Services\AgreementService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Cache;

class AgreementServiceTest extends TestCase
{

    /**
     * @test
     */
    public function testLoadMethod()
    {
        $body = json_encode([
            'sha' => sha1('test terms'),
            'content' => 'test terms'
        ]);

        $mock = new MockHandler([new Response(200, [], $body)]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        $as = new AgreementService($client);

        $result = $this->invokePrivateMethod($as, 'load', ['terms']);
        $this->assertEquals(json_decode($body), $result);
    }
}
