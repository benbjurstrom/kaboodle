@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!<br>
                        <a href="/horizon" target="_blank">Horizon</a><br>
                        <a href="/telescope" target="_blank">Telescope</a><br>
                        <a href="/clear" target="_blank">Clear Cache</a><br>
                        <a href="https://mailhog.kaboodle.dev" target="_blank">Mailhog</a><br>
                        <a href="/laravel-websockets" target="_blank">Websockets</a><br>
                        <a href="/coverage/index.html" target="_blank">Coverage</a><br>
                        <hr>
                    Last heartbeat: {{ $heartbeat }}
                        <hr>
                        <h5>Build Info:</h5>
                        {{ config('app.build.branch') }}<br>
                        {{ config('app.build.hash') }}<br>
                        {{ config('app.build.message') }}<br>
                        {{ config('app.build.datetime') }}<br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
