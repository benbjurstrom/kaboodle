@component('mail::message')
## You have a new notification!

Message: {{ $message }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
