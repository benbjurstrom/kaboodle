<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use PDO;

class DbDrop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:drop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop the postgres database with the name found in the pgsql config';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = config('database.connections.pgsql.database');
        $host = config('database.connections.pgsql.host');
        $port = config('database.connections.pgsql.port');
        $user = config('database.connections.pgsql.username');
        $pass = config('database.connections.pgsql.password');

        $db = new PDO("pgsql:host=$host;port=$port", $user, $pass, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

        try {
            $db->exec("SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '{$name}'");
            $db->exec("DROP DATABASE \"{$name}\"");
            $this->getOutput()->writeln("<info>Successfully dropped the database $name</info>");
        } catch (\Exception $e) {
            $this->getOutput()->writeln("<error>" . $e->getMessage() . "</error>");
        }
    }
}
