<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use DB;
use PDO;

class GitlabDeleteRegistry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gitlab:delete-registry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes the registry repository matching $COMMIT_BRANCH';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client([
            'base_uri' => 'https://gitlab.com/api/v4/'. env('REGISTRY_PROJECT_PATH') .'/',
            'headers' => ['PRIVATE-TOKEN' => env('REGISTRY_PASSWORD')],
        ]);

        $name = env('COMMIT_BRANCH');
        $response = $client->get('registry/repositories');
        $repositories = collect(json_decode($response->getBody()));
        $repository = $repositories->firstWhere('name', $name);

        $response = $client->delete('registry/repositories/' . $repository->id);
        $this->getOutput()->writeln("<info>Successfully deleted repository $name</info>");
    }
}
