<?php
namespace App\Services;

class ApplicationService extends \Illuminate\Foundation\Application
{
    /**
     * Determine if the application is running unit tests.
     *
     * @return bool
     */
    public function runningUnitTests()
    {
        return env('RUNNING_IN_TEST');
    }
}
