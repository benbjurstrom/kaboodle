<?php
namespace App\Services;

use App\Models\Agreement;
use App\Models\User;
use App\Models\UserAgreement;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class AgreementService
{

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function userHasAgreedToAll(User $user): bool
    {
        return $this->userHasAgreedToTerms($user) && $this->userHasAgreedToPrivacy($user);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function userHasAgreedToTerms(User $user): bool
    {
        $terms_sha = $this->getTermsAgreement()->sha;
        return $user->agreements()->where('sha', $terms_sha)->exists();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function userHasAgreedToPrivacy(User $user): bool
    {
        $privacy_sha = $this->getPrivacyAgreement()->sha;
        return $user->agreements()->where('sha', $privacy_sha)->exists();
    }

    /**
     * @return mixed
     */
    public function getTermsAgreement()
    {
        return Agreement::where('type', 'TERMS')->firstOrFail();
    }

    /**
     * @return mixed
     */
    public function getPrivacyAgreement()
    {
        return Agreement::where('type', 'PRIVACY')->firstOrFail();
    }

    /**
     * @param string $type
     * @return mixed
     * @throws
     */
    protected function load($type)
    {
        $response = $this->client->request('GET', $this->getUri($type));
        $body = $response->getBody();
        $result = json_decode($body->getContents());
        return $result;
    }

    /**
     * @param string $type
     * @return string
     */
    protected function getUri($type)
    {
        $base  = 'https://api.github.com/repos/';
        $repo  = config('terms.repository');
        $files = config('terms.files');

        return $base . $repo . '/contents/' . $files[$type];
    }
}
