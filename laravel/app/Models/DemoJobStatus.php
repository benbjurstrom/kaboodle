<?php

namespace App\Models;

use BenBjurstrom\EloquentPostgresUuids\HasUuid;
use Illuminate\Database\Eloquent\Model;

class DemoJobStatus extends Model
{
    use HasUuid;
}
