<?php

namespace App\Models;

use BenBjurstrom\EloquentPostgresUuids\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    use HasUuid;
}
