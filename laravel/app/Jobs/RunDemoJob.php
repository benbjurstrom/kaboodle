<?php

namespace App\Jobs;

use App\Models\DemoJob as DemoJobModel;
use App\Models\DemoJobStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RunDemoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var DemoJobModel
     */
    public $demoJob;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(DemoJobModel $djm)
    {
        $this->demoJob=$djm;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $djs = DemoJobStatus::where('code', 'IN_PROGRESS')->firstOrfail();
        $this->demoJob->status_id = $djs->id;
        $this->demoJob->save();

        if (!env('RUNNING_IN_TEST')) {
            sleep(rand(5, 15));
        }

        $djs = DemoJobStatus::where('code', 'COMPLETE')->firstOrfail();
        $this->demoJob->status_id = $djs->id;
        $this->demoJob->save();
    }
}
