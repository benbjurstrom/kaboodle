<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AgreementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $data = [
            'id'        => $this->id,
            'sha'       => $this->sha,
            'type'      => $this->type,
        ];

        if($request->include_content){
            $data['content'] = $this->content;
        }

        return $data;
    }
}
