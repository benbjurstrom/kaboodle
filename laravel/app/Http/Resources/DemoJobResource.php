<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DemoJobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request = null)
    {
        return [
            'id'            => $this->id,
            'user_id'       => $this->user_id,
            'status_id'     => $this->user_id,
            'status_code'   => $this->status->code,
            'created_at'    => $this->created_at->toIso8601String()
        ];
    }
}
