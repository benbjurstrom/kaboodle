<?php

namespace App\Http\Middleware;

use Closure;

class AccountEnabled
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if (!$user || !$user->enabled) {
            abort(403, 'Your account has been suspended.');
        }

        return $next($request);
    }
}
