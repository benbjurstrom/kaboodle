<?php

namespace App\Http\Middleware;

use App\Services\AgreementService;
use Closure;

class AcceptedAgreements
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @throws
     */
    public function handle($request, Closure $next)
    {
        $ts = app()->make(AgreementService::class);
        if (!$request->user() || !$ts->userHasAgreedToAll($request->user())) {
            abort(403, 'You have not accepted the latest user agreements.');
        }

        return $next($request);
    }
}
