<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    /**
     * Create a new controller instance.
     * @param bool $throttled
     * @return void
     */
    public function __construct($throttled = false)
    {
        if (env('RUNNING_IN_TEST')) {
            return;
        }

        /**
         * TODO: Setup something that works with selenium.
         *
         * Difficult because any env variables set by the test runner
         * won't exist during the api call's process.
         * */
        if (app()->environment(['container'])) {
            return;
        }

        if ($throttled) {
            $this->middleware('throttle:10,5');
        }
    }
}
