<?php

namespace App\Http\Controllers\Api\V1\Demo;

use App\Http\Resources\NotificationResource;
use App\Notifications\DemoNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $user = auth()->user();
        $jobs = $user->notifications()->orderBy('created_at', 'DESC')->get();

        return NotificationResource::collection($jobs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws ValidationException;
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'message'      => 'required|string|min:3',
        ]);

        $user = auth()->user();
        $user->notify(new DemoNotification($data['message']));

        return response()->json(null, 202);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
