<?php

namespace App\Http\Controllers\Api\V1\Demo;

use App\Http\Resources\DemoJobResource;
use App\Jobs\RunDemoJob;
use App\Models\DemoJob;
use App\Models\DemoJobStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $user = auth()->user();
        $jobs = $user->demoJobs()->orderBy('created_at', 'DESC')->get();

        return DemoJobResource::collection($jobs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        $djs = DemoJobStatus::where('code', 'PENDING')->firstOrfail();
        $dj = new DemoJob();
        $dj->status_id = $djs->id;
        $dj->user_id  = $user->id;
        $dj->save();

        // $dj = $user->demoJobs()->create(['status_id' => $djs->id]);

        dispatch(new RunDemoJob($dj));

        return (new DemoJobResource($dj))
            ->response()
            ->setStatusCode(201);
    }
}
