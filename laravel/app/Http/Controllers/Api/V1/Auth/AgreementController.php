<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Resources\AgreementResource;
use App\Models\Agreement;
use App\Services\AgreementService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AgreementController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'include_content'       => 'nullable|boolean',
        ]);

        return AgreementResource::collection(Agreement::all());
    }
}
