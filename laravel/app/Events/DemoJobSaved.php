<?php

namespace App\Events;

use App\Http\Resources\DemoJobResource;
use App\Models\DemoJob;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

/**
 * Implements ShouldBroadcastNow to ensure updates are pushed in realtime. If
 * queued model updates can end up stuck behind long running jobs.
 *
 * @package App\Events
 */
class DemoJobSaved implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var DemoJob
     */
    public $demoJob;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(DemoJob $dj)
    {
        $this->demoJob = $dj;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('App.User.' . $this->demoJob->user_id)
        ];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return (new DemoJobResource($this->demoJob))->toArray();
    }
}
