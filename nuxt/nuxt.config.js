const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Global Middleware
  */
  router: {
    middleware: ['auth', 'verified']
  },

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/app.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vee-validate.js',
    '@plugins/filters.js',
    { src: '@/plugins/axios.js', mode: 'client' }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/proxy',
    '@nuxtjs/style-resources',
    // Doc: https://buefy.github.io/#/documentation
    ['nuxt-buefy', {
      css: false,
      materialDesignIcons: true
    }],
    ['nuxt-env', {
      keys: [
        'API_HOST_INTERNAL',
        'APP_ENV',
        'COMMIT_HASH',
        'COMMIT_BRANCH',
        'COMMIT_DATETIME',
        'COMMIT_MESSAGE',
        'WEBSOCKET_KEY',
        'WEBSOCKET_HOST_EXTERNAL',
        'WEBSOCKET_PORT_EXTERNAL'
      ]
    }]
  ],

  styleResources: {
    scss: [
      './assets/scss/*.scss'
    ]
  },

  /*
  ** Axios module configuration
  */
  axios: {
    proxy: true
    // proxyHeaders: false
  },
  proxy: {
    '/v1': 'http://' + process.env.API_HOST_INTERNAL
  },

  /*
  ** Auth module configuration
  */
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/v1/auth/token', method: 'post', propertyName: 'access_token' },
          logout: { url: '/v1/auth/token', method: 'delete' },
          user: { url: '/v1/auth/user', method: 'get', propertyName: 'data' }
        }
      }
    },
    plugins: [
      // declare plugins here if you need to access context.app.$auth
      { src: '@/plugins/vue-echo.js', ssr: false }
    ],
    redirect: {
      login: '/login',
      logout: '/login',
      home: '/home'
    }
  },

  /*
  ** Build configuration
  */
  build: {
    devtools: true,
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            emitError: true,
            emitWarning: true,
            failOnError: true,
            failOnWarning: true
          }
        })
      }
    }
  },

  watchers: {
    webpack: {
      aggregateTimeout: 300,
      poll: 1000
    }
  }
}
