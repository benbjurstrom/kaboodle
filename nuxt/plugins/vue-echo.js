import Vue from 'vue'
import Echo from 'laravel-echo'
import { Snackbar } from 'buefy/dist/components/snackbar'

export default ({ app, store, env }) => {
  echoConnect(app.$auth, store, env)

  function echoConnect (auth, store) {
    if (!auth.loggedIn) {
      return
    }

    window.Pusher = require('pusher-js')

    const token = auth.getToken('local')
    const userId = auth.user.id

    const options = {
      broadcaster: 'pusher',
      key: store.state.env.websocketKey,
      forceTLS: false,
      wsHost: store.state.env.websocketHostExternal,
      wsPort: store.state.env.websocketPortExternal,
      disableStats: true,
      authEndpoint: 'v1/broadcasting/auth',
      auth: {
        headers: {
          Authorization: token,
          Accept: 'application/json'
        }
      }
    }

    const echo = new Echo(options)
    echo.private(`App.User.${userId}`)
      .notification((data) => {
        console.log(data)
        Snackbar.open({
          message: `You have a new notification!<br>Message:<em> ${data.message}</em><br>Type: <em>${data.type}</em>.`
        })
      })
      .listen('DemoJobSaved', (data) => {
        store.commit('jobs/UPDATE', data)
      })
  }

  Vue.prototype.$echoConnect = echoConnect
}
