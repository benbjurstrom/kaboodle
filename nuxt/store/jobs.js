import { getField, updateField } from 'vuex-map-fields'

const initialState = {
  data: [
    // {
    //   id: null,
    //   status_id: null,
    //   status_code: null,
    //   created_at: null,
    // }
  ],
  form: {
    name: null
  }
}

export default {
  namespaced: true,
  state: () => (JSON.parse(JSON.stringify(initialState))),
  getters: {
    getField
  },
  actions: {
    async getCollection ({ commit, state }, config) {
      const { data } = await this.$axios.get('v1/demo/jobs', config)
      commit('SET', data)
    },
    async create ({ commit, state }, config) {
      await this.$axios.post('v1/demo/jobs', {
        ...state.form
      }, config)
      commit('RESET_FORM')
    }
  },
  mutations: {
    SET: (state, data) => {
      state.data = data.data
    },
    RESET_FORM (state) {
      Object.assign(state.form, initialState.form)
    },
    UPDATE (state, record) {
      const index = state.data.findIndex(el => el.id === record.id)

      /**
       * Return early if the record is not found. This could be because the record is
       * new or on a different page of a paginated collection
       */
      if (index === -1) {
        return
      }

      Object.assign(state.data[index], record)
    },
    updateField
  }
}
