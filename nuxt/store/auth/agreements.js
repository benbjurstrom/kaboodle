const initialState = {
  data: {
    terms: {
      sha: null,
      content: null
    },
    privacy: {
      sha: null,
      content: null
    }
  }
}

export default {
  namespaced: true,
  state: () => (JSON.parse(JSON.stringify(initialState))),
  actions: {
    async get ({ commit }, config) {
      const { data } = await this.$axios.get('v1/auth/agreements', config)
      commit('SET', data.data)
    },
    async getWithContent ({ commit }, config) {
      const { data } = await this.$axios.get('v1/auth/agreements?include_content=1', config)
      commit('SET', data.data)
    }
  },
  mutations: {
    SET: (state, data) => {
      console.log(data)
      const privacy = data.find(obj => {
        return obj.type === 'PRIVACY'
      })

      const terms = data.find(obj => {
        return obj.type === 'TERMS'
      })

      console.log(terms)

      state.data.terms.sha = terms.sha
      state.data.privacy.sha = privacy.sha

      if (terms.content) {
        state.data.terms.content = terms.content
      }

      if (privacy.content) {
        state.data.privacy.content = privacy.content
      }
    },
    RESET (state) {
      Object.assign(state, initialState)
    }
  }
}
