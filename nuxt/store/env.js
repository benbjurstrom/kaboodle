import { getField } from 'vuex-map-fields'

const initialState = {
  apiHostInternal: null,
  appEnv: null,

  commitHash: '0000000000000000000000000000000000000000',
  commitBranch: 'default branch',
  commitDatetime: '2019-07-05 13:10:52 -0700',
  commitMessage: 'default message',

  websocketKey: null,
  websocketHostExternal: null,
  websocketPortExternal: null
}

export default {
  namespaced: true,
  state: () => (JSON.parse(JSON.stringify(initialState))),
  getters: {
    getField
  },
  actions: {
    setEnv ({ commit }) {
      commit('apiHostInternal', this.$env.API_HOST_INTERNAL)
      commit('appEnv', this.$env.APP_ENV)

      commit('commitHash', this.$env.COMMIT_HASH)
      commit('commitBranch', this.$env.COMMIT_BRANCH)
      commit('commitDatetime', this.$env.COMMIT_DATETIME)
      commit('commitMessage', this.$env.COMMIT_MESSAGE)

      commit('websocketKey', this.$env.WEBSOCKET_KEY)
      commit('websocketHostExternal', this.$env.WEBSOCKET_HOST_EXTERNAL)
      commit('websocketPortExternal', this.$env.WEBSOCKET_PORT_EXTERNAL)
    }
  },
  mutations: {
    apiHostInternal: (state, data) => {
      state.apiHostInternal = data
    },

    appEnv: (state, data) => {
      state.appEnv = data
    },

    commitHash: (state, data) => {
      if (!data) return
      state.commitHash = data
    },
    commitBranch: (state, data) => {
      if (!data) return
      state.commitBranch = data
    },
    commitDatetime: (state, data) => {
      if (!data) return
      state.commitDatetime = data
    },
    commitMessage: (state, data) => {
      if (!data) return
      state.commitMessage = data
    },

    websocketKey: (state, data) => {
      state.websocketKey = data
    },
    websocketHostExternal: (state, data) => {
      state.websocketHostExternal = data
    },
    websocketPortExternal: (state, data) => {
      state.websocketPortExternal = data
    }
  }
}
